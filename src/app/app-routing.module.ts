import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { DefaultComponent } from './layouts/default/default.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { MarkdownComponent } from './modules/markdown/markdown.component';
import { PostsComponent } from './modules/posts/posts.component';

const routes: Routes = [{
  path: '',
  component: DefaultComponent,
  children: [
    { path: '', component: DashboardComponent }, 
    { path: 'posts', component: PostsComponent }, 
    { path: 'markdown', component: MarkdownComponent }
  ]
}]

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
