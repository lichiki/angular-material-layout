import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-markdown',
  templateUrl: './markdown.component.html',
  styleUrls: ['./markdown.component.css']
})
export class MarkdownComponent implements OnInit {

  md;
  @Input() name: string;
  constructor(private http:HttpClient) { }

  async ngOnInit(){
    this.md = await this.http.get(`/assets/md/test.md`, { responseType: 'text'}).toPromise();
  }

}
