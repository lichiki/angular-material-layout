# Angular Material Layout

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


## Build for tomcat
# Sets base tag href to /myUrl/ in your index.html
ng build --base-href /myUrl/
ng build -bh /myUrl/

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Use jQuery
add jquery
./angular.json 
"scripts": [ "../node_modules/jquery/dist/jquery.min.js" ] 

import * as $ from 'jquery';
(or)
declare var $: any;

## Update Angular to last version
ng update @angular/cli @angular/core @angular/flex-layout @angular/material @angular/cdk

## NPM Config goalbal path
- check global path install node library default by Command
    npm config get prefix
- set new global path install node library by Command
    npm config set prefix D:\DevTools\npm_global
- add global path to Environment valiables path
D:\DevTools\npm_global

## Proxy reserve port for cross origin
create src/proxy.cont.json
    {
        "/cas-apis": {
            "target": "http://localhost:8080",
            "secure": false
        }
    }
Modify angular.json 
    "serve": {
          "builder": "@angular-devkit/build-angular:dev-server",
          "options": {
            "browserTarget": "system-e:build",
            "proxyConfig": "src/proxy.conf.json"
          },